var express = require('express');
var router = express.Router();

var codigos = require('../public/utilidades');
var personaCtrl = require('../controlador/personaControl');
var persona = new personaCtrl;

var cuentaControl = require('../controlador/cuentaControlador');
var cuenta = new cuentaControl;

var estudianteControl = require('../controlador/estudianteControlador');
var estudiante = new estudianteControl;

var adminControlador = require('../controlador/adminControlador');
var admin = new adminControlador;

var docenteControlador = require('../controlador/docenteControlador');
var docente = new docenteControlador;

var labControl = require('../controlador/labControlador');
var lab = new labControl;

/*----------auth----------*/
//middleware adminstrador
var auth_adm = function (req, res, next) {
    if (!req.session.usuario) {
        var sess = req.session.usuario;
        console.log('entro al auth');
        console.log(sess);
        req.flash('error', 'Necesitas iniciar sesion');
        res.redirect('/');
    } else if(req.session.usuario.cargo !== 'admin'){
        var sess = req.session.usuario;
        console.log('entro al auth');
        console.log(sess);
        req.flash('error', 'Necesitas iniciar sesion');
        res.redirect('/main');
    }else{
        next();
    }
};

//middleware docente
var auth_dct = function (req, res, next) {
    if (!req.session.usuario) {
        var sess = req.session.usuario;
        console.log('entro al auth');
        console.log(sess);
        req.flash('error', 'Necesitas iniciar sesion');
        res.redirect('/');
    } else if(req.session.usuario.cargo !== 'docente'){
        var sess = req.session.usuario;
        console.log('entro al auth');
        console.log(sess);
        req.flash('error', 'Necesitas iniciar sesion');
        res.redirect('/main');
    }else{
        next();
    }
};
//middleware alumno
var auth_std = function (req, res, next) {
    if (!req.session.usuario) {
        var sess = req.session.usuario;
        console.log('entro al auth');
        console.log(sess);
        req.flash('error', 'Necesitas iniciar sesion');
        res.redirect('/');
    } else if(req.session.usuario.cargo !== 'admin'){
        var sess = req.session.usuario;
        console.log('entro al auth');
        console.log(sess);
        req.flash('error', 'Necesitas iniciar sesion');
        res.redirect('/main');
    }else{
        next();
    }
};

/*----------Fin auth----------*/

/*----------VISTAS principales----------*/
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('base', {title: 'Laboratorios UNL', fragmentos: 'index'});
});
/* GET iniciar sesion */
router.get('/iniciar', function (req, res, next) {
    res.render('base', {title: 'log in', fragmentos: 'inicio'});
});

router.get('/crear_clase', auth_dct, function (req, res) {
    res.render('base', {
        title: 'Crear Clase ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'docente/nuevaclase',
        usuario: req.session.usuario.nombre
    });
});

router.get('/registrar_estudiante', auth_adm, function (req, res) {
    res.render('base', {
        title: 'Nuevo estudiante ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'administrador/adminEstudiante',
        usuario: req.session.usuario.nombre
    });
});
router.get('/registrar_docente', auth_adm, function (req, res) {
    res.render('base', {
        title: 'Nuevo estudiante ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'administrador/adminDocente',
        usuario: req.session.usuario.nombre
    });
});

router.get('/crear_lab', auth_adm, function (req, res) {
    res.render('base', {
        title: 'Nuevo laboratorio ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'administrador/labregistro',
        usuario: req.session.usuario.nombre
    });
});

router.get('/lista_labs', auth_adm, function (req, res) {
    res.render('base', {
        title: 'Lista laboratorios ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'administrador/listadolabs',
        usuario: req.session.usuario.nombre
    });
});

router.get('/estaciones', auth_adm, function (req, res) {
    res.render('base', {
        title: 'Lista laboratorios ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'administrador/lista_estac',
        usuario: req.session.usuario.nombre
    });
});

router.get('/componentes', auth_adm , function (req, res) {
    res.render('base', {
        title: 'Lista laboratorios ',
        styles: codigos.codigos,
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: req.session.usuario,
        fragmentos: 'administrador/adminComp',
        usuario: req.session.usuario.nombre
    });
});





//Vistas sin ventana
/*********************************************************************************
router.post('/crear_clase', docente.crear_clase );
router.get('/lista_asistencia', docente.asistencia );

router.post('/guardar', admin.guardar_admin);
router.post('/registrar_estudiante', estudiante.registrar_estudiante);

router.get('/listadmin', auth_adm, admin.enlistar_admins);

**********************************************************************************/ 
/*----------FIN VISTAS docentes----------*/




/*----------FIN VISTAS administrador----------*/
/* GET tabla de registro */
router.get('/registro',  function(req, res, next) {
    res.render('partials/registro', { title: 'Registro de administradores' });
  });



/* GET sincronisacionde la base de datos */
router.get('/sinc', function (req, res, next) {
    var models = require('../models/');
    models.sequelize.sync().then(() => {
        console.log('Se ha sincronisado la base de datos');
        res.send('Se ha sincronisado la bd');
    }).catch(err => {
        console.log(err, 'Hubo un error');
        res.send('No se pudo sincronizar la bd');
    });
});

/*GET cerrar sesion */
router.get('/cerrar_log', cuenta.cerrar_sesion);

router.post('/registrate', persona.guardar);

router.post('/main', cuenta.inicio);

router.post('/admin/crear_clase', cuenta.inicio);

router.get('/main', function (req, res) {
    res.render('base', {
        title: 'Principal ',
        botones: 'pantalla/botones'+req.session.usuario.cargo,
        sesion: true,
        fragmentos: req.session.usuario.frag,
        usuario: req.session.usuario.nombre
    });
});
module.exports = router;
