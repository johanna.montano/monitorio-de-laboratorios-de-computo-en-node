'use strict';
module.exports = (sequelize, DataTypes) => {
  const persona = sequelize.define('persona', {
    cedula: DataTypes.STRING,
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING,
    cargo: DataTypes.INTEGER,
    external: DataTypes.UUID
  }, {freezeTableName: true});
  persona.associate = function(models) {
    // associations can be defined here
    persona.hasOne(models.cuenta,{foreignKey:'id_persona', as: 'cuenta'});
    persona.hasMany(models.clase,{foreignKey:'id_persona', as: 'clase'});
    persona.hasMany(models.clase_asistencia,{foreignKey:'id_persona', as: 'clase_asistencia'});
  };
  return persona;
};