'use strict';
module.exports = (sequelize, DataTypes) => {
  const componente = sequelize.define('componente', {
    presente: DataTypes.STRING,
    observaciones: DataTypes.STRING,
    marca: DataTypes.STRING,
    serie: DataTypes.STRING,
    codigo: DataTypes.STRING,
    nombre: DataTypes.STRING,
    disponibilidad: DataTypes.BOOLEAN,
    external: DataTypes.UUID
  }, {freezeTableName: true});
  componente.associate = function(models) {
    // associations can be defined here
    componente.belongsTo(models.estacionesTrabajo, {foreignKey: 'id_estacionesTrabajo'});
  };
  return componente;
};