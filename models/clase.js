'use strict';
module.exports = (sequelize, DataTypes) => {
  const clase = sequelize.define('clase', {
    tema: DataTypes.STRING,
    observaciones: DataTypes.STRING,
    curso: DataTypes.INTEGER,
    paralelo: DataTypes.STRING,
    asignatura: DataTypes.STRING,
    nro_practica:DataTypes.INTEGER,
    external: DataTypes.UUID
  }, {freezeTableName: true});
  clase.associate = function(models) {
    // associations can be defined here
    clase.belongsTo(models.persona, {foreignKey: 'id_persona'});
    clase.hasMany(models.clase_asistencia,{foreignKey:'id_clase', as: 'clase_asistencia'});
    clase.belongsTo(models.laboratorio, {foreignKey: 'id_laboratorio'});
  };
  return clase;
};