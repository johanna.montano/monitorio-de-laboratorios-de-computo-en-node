'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('componentes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      presente: {
        type: Sequelize.STRING
      },
      observaciones: {
        type: Sequelize.STRING
      },
      estado: {
        type: Sequelize.STRING
      },
      marca: {
        type: Sequelize.STRING
      },
      serie: {
        type: Sequelize.STRING
      },
      codigo: {
        type: Sequelize.STRING
      },
      disponibilidad: {
        type: Sequelize.BOOLEAN
      },
      external: {
        type: Sequelize.UUID
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('componentes');
  }
};