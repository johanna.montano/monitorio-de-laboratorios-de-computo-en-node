'use strict';
var models = require('./../models/');
var persona = models.persona;
const bcrypt = require('bcrypt');

class cuentaControlador {
    inicio(req, res) {
        persona.findAll({ include: [{ model: models.cuenta, as: 'cuenta', where: { correo: req.body.correoi } }] }).then(function (arre) {
            if (arre.length > 0) {
                var result = arre[0];
                var cuentasP = result.cuenta;
                var userPassword = req.body.contasena;
                bcrypt.compare(userPassword, cuentasP.clave, function (err, esCorrecta) {
                    if (err) {
                        return res.send('error interno');
                    }
                    if (esCorrecta) {
                        var cargos = '';
                        var fragPrincipal = '';
                        switch (result.cargo) {
                            case 1:
                                cargos = 'estudiante';
                                fragPrincipal = 'estudiante/prestudiante';
                                break;
                            case 2:
                                cargos = 'docente';
                                fragPrincipal = 'docente/vistadocente';
                                break;
                            case 3:
                                cargos = 'admin';
                                fragPrincipal = 'administrador/admin';
                        }
                        req.session.usuario = {
                            external: result.external,
                            nombre: result.nombre + ' ' + result.apellido,
                            sesion: req.session.usuario,
                            frag: fragPrincipal
                        };
                        res.render('base', {
                            title: 'Principal ',
                            botones: 'pantalla/botones' + cargos,
                            sesion: true,
                            fragmentos: req.session.usuario.frag,
                            usuario: req.session.usuario.nombre
                        });
                        console.log('aparentemente ya esta iniciado sesion');
                        console.log.apply(req.session.usuario);

                        /*Datos de la cuenta (modificar despues con passport)*/
                    } else {
                        console.log('ingrese credenciales validos');
                        res.redirect('/iniciar');
                    }
                });
            } else {
                console.log('la cuenta no existe 2');
                res.redirect('/iniciar');
            }
        }).error(function (error) {
            console.log('la cuenta no existe 1');
            console.log(error);
            res.redirect('/iniciar');
        });
    }


    cerrar_sesion(req, res) {
        req.session.destroy();
        res.redirect('/');
    }
}
module.exports = cuentaControlador;