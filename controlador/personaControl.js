'use strict';
var models = require('./../models/');
const bcrypt = require('bcrypt');

class personaControlador {
    guardar(req, res) {
        bcrypt.hash(req.body.pword, 10, function (err, hash) {
            if (err) {
                console.log(err);
                return res.redirect("/");
            }

            console.log('Contraseña original: ' + req.body.pword);
            console.log('Contraseña encriptada: ' + hash);

            var uuid = require('uuid');
            var persona = models.persona;
            var datos = {
                cedula: req.body.cedula,
                nombre: req.body.nombres,
                apellido: req.body.apellidos,
                fecha_nac: req.body.fecha_n,
                cargo: req.body.cargo,
                external: uuid.v4(),
                cuenta: {
                    correo: req.body.email,
                    clave: hash,
                    estado: 1,
                    external: uuid.v4()
                }
            };

            persona.create(datos, { include: [{ model: models.cuenta, as: 'cuenta' }] }).then(function (dat) {
                //Sreq.flash('ok', "Se ha registrado Persona correctamente");
                res.redirect("/");
                console.log('si funciona :v');

            });
        });
    }
}
module.exports = personaControlador;


